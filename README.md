# CalyxOS Wallpapers

Public domain and creative commons licensed wallpapers for use with CalyxOS

Usage:

1. Edit sources.yml
2. Run ./process-images
3. See ./wallpapers directory for output

## Currently Used Sources

* https://commons.wikimedia.org/wiki/Main_Page
* https://bazaar.launchpad.net/~ubuntu-art-pkg/ubuntu-wallpapers/ubuntu/files

## Potential Sources

Source with a high degree of confidence regarding the license:

* https://commons.wikimedia.org/wiki/Main_Page
* https://bazaar.launchpad.net/~ubuntu-art-pkg/ubuntu-wallpapers/ubuntu/files
* https://en.wikipedia.org/wiki/Wikipedia:Public_domain_image_resources
* https://www.loc.gov/free-to-use/

Other OS wallpaper collections:

* https://github.com/pop-os/wallpapers
* https://github.com/Antergos/wallpapers-extra
* https://github.com/elementary/wallpapers

Other sources with questionable license validity:

* https://pixabay.com/users/publicdomainarchive-262011/
* https://unsplash.com/images/stock/public-domain
* https://www.pexels.com/public-domain-images/

## Apps

* https://github.com/danimahardhika/wallpaperboard -- heavily customizable, connect to anything using JSON API.
* https://github.com/GreyLabsDev/PexWalls -- client for pexels.com
* https://github.com/biniamHaddish/WallPack -- client for unsplash.com
* https://github.com/JuniperPhoton/MyerSplash.Android -- client for unsplash.com
* https://github.com/hoc081098/wallpaper-flutter
* https://github.com/Hash-Studios/Prism

## Device Screen Sizes

| Pixel 2     | 1080 x 1920 | 9:16   |
| Pixel 2 XL  | 1440 x 2880 | 9:18   |
| Pixel 3     | 1080 x 2160 | 9:18   |
| Pixel 3 XL  | 1440 x 2960 | 9:18.5 |
| Pixel 3a    | 1080 x 2220 | 9:18.5 |
| Pixel 3a XL | 1080 x 2160 | 9:18   |
| Pixel 4     | 1080 x 2280 | 9:19   |
| Pixel 4 XL  | 1440 x 3040 | 9:19   |
| Pixel 4a    | 1080 x 2340 | 9:19.5 |
| Mi A2       | 1080 x 2160 | 9:18   |

The maximum screen height is 3040px and the maximum width is 1440px.
